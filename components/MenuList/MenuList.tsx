import { useRouter } from 'next/router'
import Image from 'next/image'
import { useContext, useEffect, useState } from 'react'
import * as Types from 'generated/__types__'
import { CartContext, CartItemType } from 'utils/CartContext/CartContext'
import { Loading } from 'components/Loading/Loading'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { NoProductsAvailable } from 'components/NoProductsAvailable/NoProductsAvailable'
import { DishesQueryVariables, useDishesQuery } from './menu-list.graphql'
import { Currency } from 'components/Currency/Currency'
import { itemsPerPage, Pagination } from 'components/Pagination/Pagination'
import { useDishesCountLazyQuery } from './dishes-count.graphql'

type DishType = Pick<Types.Dish, 'id' | 'name' | 'description' | 'price'> & {
  image?: Types.Maybe<
    {
      __typename?: 'UploadFile'
    } & Pick<Types.UploadFile, 'url'>
  >
}

export const MenuList: React.FC = () => {
  const router = useRouter()
  const [currentPage, setCurrentPage] = useState<number>()
  const [variables, setVariables] = useState<DishesQueryVariables>()
  const { data, loading, error } = useDishesQuery({
    skip: !variables,
    variables,
  })
  const { addItemsToCart } = useContext(CartContext)
  const [totalOfItems, setTotalOfItems] = useState<number>()
  const [getItemsCount, { data: dishesCountData }] = useDishesCountLazyQuery()

  const dishToCart = (dish: DishType) => {
    if (!dish) return

    const cartItem: CartItemType = {
      id: dish.id,
      name: dish.name,
      numberOfPieces: 1,
      unitPrice: dish.price,
    }

    addItemsToCart(cartItem)
  }

  // set the current page query parameter
  useEffect(() => {
    if (router) {
      const queryPage = router.query.page || '1'

      setCurrentPage(
        parseInt(Array.isArray(queryPage) ? queryPage[0] : queryPage)
      )
    }
  }, [router])

  // set variables for query
  useEffect(() => {
    if (typeof currentPage === 'number' && typeof itemsPerPage === 'number') {
      const start = (currentPage - 1) * itemsPerPage

      setVariables({
        limit: itemsPerPage,
        start,
      })
    }
  }, [currentPage])

  // trigger get number of total items call
  useEffect(() => {
    if (data) {
      getItemsCount()
    }
  }, [data, getItemsCount])

  // set the total number of items
  useEffect(() => {
    if (typeof dishesCountData?.dishesCount === 'number') {
      setTotalOfItems(dishesCountData.dishesCount)
    }
  }, [dishesCountData])

  if (loading) {
    return <Loading />
  }

  if (error) {
    return <ErrorMessage />
  }

  if (!data || !data.dishes) {
    return <NoProductsAvailable />
  }

  return (
    <>
      {data.dishes.map((dish) => {
        if (dish) {
          const imageSrc =
            dish.image && dish.image.url
              ? process.env.NEXT_PUBLIC_IMG_ORIGIN + dish.image.url
              : ''

          return (
            <div className="flex mt-8" key={dish.id}>
              <figure className="w-96 h-64 relative bg-gray-300">
                <Image
                  alt={dish.name}
                  src={imageSrc}
                  layout="fill"
                  objectFit="cover"
                />
              </figure>
              <div>
                <header>{dish.name}</header>
                <p>{dish.description}</p>
                <div>
                  <Currency amount={dish.price} />
                </div>
                <button onClick={() => dishToCart(dish)}>add to cart</button>
              </div>
            </div>
          )
        }
      })}

      {totalOfItems && currentPage && (
        <Pagination
          currentPage={currentPage}
          itemsPerPage={itemsPerPage}
          totalOfItems={totalOfItems}
        />
      )}
    </>
  )
}
