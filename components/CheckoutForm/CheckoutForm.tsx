import { useContext, useState } from 'react'
import { useRouter } from 'next/router'
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js'
import { StripeCardElementChangeEvent } from '@stripe/stripe-js'
import { CartContext } from 'utils/CartContext/CartContext'
import { createPaymentIntent } from 'utils/Payment/createPaymentIntent'
import { getPaymentErrorMessage } from 'utils/Payment/stripe-error-codes'
import {
  PaymentStatus,
  StripePaymentStatus,
} from 'components/PaymentStatus/PaymentStatus'
import { useCreateOrderMutation } from './CreateOrder.graphql'
import * as Types from 'generated/__types__'

export const CheckoutForm: React.FC = () => {
  const router = useRouter()
  const stripe = useStripe()
  const elements = useElements()
  const { isCartEmpty, cartItems, totalAmount } = useContext(CartContext)
  const [paymentStatus, setPaymentStatus] = useState(
    StripePaymentStatus.Initial
  )
  const [errorMessage, setErrorMessage] = useState<string>()
  const [isPaymentInProgress, setIsPaymentInProgress] = useState(false)
  const [createOrderMutation] = useCreateOrderMutation()
  const handleCardElementChange = (event: StripeCardElementChangeEvent) => {
    if (event.error) {
      const errorMessage = getPaymentErrorMessage({ code: event.error.code })
      setErrorMessage(errorMessage)
      setPaymentStatus(StripePaymentStatus.Error)
    }
  }
  const handleSubmit: React.FormEventHandler<HTMLFormElement> = async (
    event
  ) => {
    const cardElement = elements?.getElement(CardElement)

    event.preventDefault()
    setPaymentStatus(StripePaymentStatus.Initial)

    if (!cartItems || !stripe || !cardElement) {
      return false
    }

    setIsPaymentInProgress(true)

    // create payment intent
    const paymentIntent = await createPaymentIntent({ payload: cartItems })

    if (!paymentIntent || !paymentIntent.clientSecret) {
      setPaymentStatus(StripePaymentStatus.Error)
      setIsPaymentInProgress(false)

      return false
    }

    const confirmPaymentResponse = await stripe.confirmCardPayment(
      paymentIntent.clientSecret,
      {
        payment_method: {
          card: cardElement,
          // TODO
          // billing_details
        },
      }
    )

    if (confirmPaymentResponse.error) {
      setPaymentStatus(StripePaymentStatus.Error)
      setIsPaymentInProgress(false)

      return false
    }

    const createOrderResponse = await createOrderMutation({
      variables: {
        input: {
          data: {
            cart: cartItems,
            paymentMethod: Types.Enum_Order_Paymentmethod.Card,
            paymentIntentId: paymentIntent.id,
            totalAmount,
          },
        },
      },
    })

    setIsPaymentInProgress(false)

    if (createOrderResponse.errors) {
      setPaymentStatus(StripePaymentStatus.Error)
    } else {
      setPaymentStatus(StripePaymentStatus.Succeeded)
      router.push(
        `/order-success/${createOrderResponse.data?.createOrder?.order?.id}`
      )
    }
  }

  if (isCartEmpty) {
    return <>The cart is empty</>
  }

  return (
    <>
      <form className="max-w-sm mx-auto" onSubmit={handleSubmit}>
        <CardElement onChange={handleCardElementChange} />
        {stripe && (
          <button
            type="submit"
            className="bg-black text-white px-4 rounded disabled:opacity-50 disabled:cursor-default"
            disabled={
              ![
                StripePaymentStatus.Initial,
                StripePaymentStatus.Succeeded,
                StripePaymentStatus.Error,
              ].includes(paymentStatus) || isPaymentInProgress
            }
          >
            {isPaymentInProgress ? 'Loading...' : 'Pay'}
          </button>
        )}
      </form>

      <PaymentStatus status={paymentStatus} errorMessage={errorMessage} />
    </>
  )
}
