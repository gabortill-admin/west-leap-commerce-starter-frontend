import { useContext } from 'react'
import { CartContext } from 'utils/CartContext/CartContext'
import { Currency } from 'components/Currency/Currency'

export const CartList: React.FC = () => {
  const { cartItems, isCartEmpty, totalAmount } = useContext(CartContext)

  if (isCartEmpty) {
    return <>The cart is empty</>
  }

  return (
    <>
      {cartItems.map((item) => {
        return (
          <div key={item.id}>
            {item.name} {item.numberOfPieces} x{' '}
            <Currency amount={item.unitPrice} />
          </div>
        )
      })}
      <hr />
      <div>
        total: <Currency amount={totalAmount} />
      </div>
    </>
  )
}
