export const ErrorMessage: React.FC = ({ children }) => {
  if (children) {
    return <div>{children}</div>
  }

  return <div>An error happened 😔</div>
}
