import Link from 'next/link'
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'

type PaginationProps = {
  currentPage: number
  itemsPerPage: number
  totalOfItems: number
}

export const itemsPerPage = 4

export const Pagination = ({
  currentPage,
  itemsPerPage,
  totalOfItems,
}: PaginationProps) => {
  const router = useRouter()
  const prevPage = currentPage - 1
  const nextPage = currentPage + 1
  const [numberOfPages, setNumberOfPages] = useState<number>()
  const [pages, setPages] = useState<number[]>([])

  // set the maximum number of available pages
  useEffect(() => {
    setNumberOfPages(Math.ceil(totalOfItems / itemsPerPage))
  }, [currentPage, itemsPerPage, totalOfItems])

  // set the available page number to display them
  useEffect(() => {
    if (typeof numberOfPages === 'number') {
      const pages = []

      for (let index = 0; index < numberOfPages; index++) {
        pages.push(index + 1)
      }

      setPages(pages)
    }
  }, [numberOfPages])

  // go to the last page if
  // the current page number is higher than the max available
  useEffect(() => {
    if (
      typeof numberOfPages === 'number' &&
      router &&
      currentPage > numberOfPages
    ) {
      router.push({
        query: { page: numberOfPages },
      })
    }
  }, [currentPage, numberOfPages, router])

  if (!pages || typeof numberOfPages !== 'number' || numberOfPages === 1) {
    return null
  }

  return (
    <>
      {currentPage > 1 && (
        <Link href={{ query: currentPage === 2 ? {} : { page: prevPage } }}>
          previous
        </Link>
      )}

      {pages.map((pageNumber) => {
        const isCurrentPage = pageNumber === currentPage

        if (isCurrentPage) {
          return <span key={pageNumber}>{pageNumber}</span>
        }

        return (
          <Link key={pageNumber} href={{ query: { page: pageNumber } }}>
            <a>{pageNumber}</a>
          </Link>
        )
      })}

      {currentPage < numberOfPages && (
        <Link href={{ query: { page: nextPage } }}>next</Link>
      )}
    </>
  )
}
