import { Header } from 'components/Header/Header'
import { ReactNode } from 'react'

type LayoutProps = {
  isSimplified?: boolean
  children: ReactNode
  className?: string
}

export const Layout = ({
  isSimplified = false,
  children,
  className,
}: LayoutProps) => {
  return (
    <div className={className}>
      <Header isSimplified={isSimplified} />
      {children}
    </div>
  )
}
