import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'

export enum StripePaymentStatus {
  Processing = 'processing',
  RequiresPaymentMethod = 'requires_payment_method',
  RequiresConfirmation = 'requires_confirmation',
  RequiresAction = 'requires_action',
  Succeeded = 'succeeded',
  Error = 'error',
  Initial = 'initial',
}

export type PaymentStatusProps = {
  status: StripePaymentStatus
  errorMessage?: string
}

export const PaymentStatus = ({ status, errorMessage }: PaymentStatusProps) => {
  switch (status) {
    case StripePaymentStatus.Processing:
    case StripePaymentStatus.RequiresPaymentMethod:
    case StripePaymentStatus.RequiresConfirmation:
      return <div>Processing...</div>

    case StripePaymentStatus.RequiresAction:
      return <div>Authenticating...</div>

    case StripePaymentStatus.Succeeded:
      return <div>Payment Succeeded 🥳</div>

    case StripePaymentStatus.Error:
      return <ErrorMessage>{errorMessage}</ErrorMessage>

    case StripePaymentStatus.Initial:
    default:
      return null
  }
}
