export const NoProductsAvailable = (): JSX.Element => {
  return <div>No products available</div>
}
