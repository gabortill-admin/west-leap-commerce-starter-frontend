import currency from 'currency.js'

export const Currency = ({ amount }: { amount: number }): JSX.Element => {
  const isWholeNumber = amount - Math.floor(amount) === 0

  return (
    <>
      {currency(amount, {
        precision: isWholeNumber ? 0 : 2,
        symbol: '€ ',
        separator: '.',
        decimal: ',',
      }).format()}
    </>
  )
}
