import Link from 'next/link'
import { useContext } from 'react'
import { CartContext } from 'utils/CartContext/CartContext'

type HeaderProps = {
  isSimplified?: boolean
}

export const Header = ({ isSimplified = false }: HeaderProps) => {
  const { totalNumberOfItemsInCart } = useContext(CartContext)

  return (
    <header>
      <div>Logo</div>
      {!isSimplified && (
        <Link href="/cart">
          <a>cart: {totalNumberOfItemsInCart}</a>
        </Link>
      )}
    </header>
  )
}
