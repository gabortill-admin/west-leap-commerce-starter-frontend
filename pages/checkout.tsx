import { NextPage } from 'next'
import { Layout } from 'components/Layout/Layout'
import { Elements } from '@stripe/react-stripe-js'
import getStripe from 'utils/getStripe/getStripe'
import { CheckoutForm } from 'components/CheckoutForm/CheckoutForm'

const CheckoutPage: NextPage = () => {
  return (
    <Layout isSimplified={true}>
      <Elements stripe={getStripe()}>
        <CheckoutForm />
      </Elements>
    </Layout>
  )
}

export default CheckoutPage
