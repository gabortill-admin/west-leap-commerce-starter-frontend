import { NextPage } from 'next'
import { Layout } from 'components/Layout/Layout'

const Custom404: NextPage = () => {
  return <Layout>404</Layout>
}

export default Custom404
