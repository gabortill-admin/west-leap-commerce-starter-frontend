import Link from 'next/link'
import { CartList } from 'components/CartList/CartList'
import { Layout } from 'components/Layout/Layout'

export default function CartPage(): JSX.Element {
  return (
    <Layout isSimplified={true}>
      <CartList />
      <Link href="/checkout">
        <a>Go to checkout</a>
      </Link>
    </Layout>
  )
}
