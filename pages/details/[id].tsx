import Image from 'next/image'
import { NextPage } from 'next'
import { useRouter } from 'next/router'
import { Layout } from 'components/Layout/Layout'
import { DishQueryVariables, useDishQuery } from './dish.graphql'
import { Loading } from 'components/Loading/Loading'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { extractId } from 'utils/extract-id'
import { useEffect, useState } from 'react'
import { Currency } from 'components/Currency/Currency'

const ProductDetailPage: NextPage = () => {
  const router = useRouter()
  const [variables, setVariables] = useState<DishQueryVariables>()
  const [isIdMissing, setIsIdMissing] = useState<boolean>()
  const [imageSrc, setImageSrc] = useState<string>()
  const { data, loading, error } = useDishQuery({
    variables,
    skip: !variables,
  })

  // get id from query params and set variables for query
  useEffect(() => {
    if (router && router.query.id) {
      const id = extractId(
        Array.isArray(router.query.id) ? router.query.id[0] : router.query.id
      )

      if (id === null) {
        setIsIdMissing(true)
      } else {
        setVariables({ id })
      }
    }
  }, [router])

  // redirect in case of missing id
  useEffect(() => {
    if (isIdMissing) {
      router.push('/404')
    }
  }, [isIdMissing, router])

  // set image source
  useEffect(() => {
    if (data?.dish?.image?.url) {
      setImageSrc(process.env.NEXT_PUBLIC_IMG_ORIGIN + data.dish.image.url)
    }
  }, [data?.dish?.image?.url])

  if (loading) {
    return (
      <Layout>
        <Loading />
      </Layout>
    )
  }

  if (error) {
    const isMissingProduct = error.message
      .toLocaleLowerCase()
      .startsWith('cast to objectid failed')

    return (
      <Layout>
        {isMissingProduct ? <div>no such product</div> : <ErrorMessage />}
      </Layout>
    )
  }

  return (
    <Layout>
      <h1>{data?.dish?.name}</h1>
      <div>{data?.dish?.description}</div>
      <Currency amount={data?.dish?.price || 0} />
      {imageSrc && (
        <figure className="w-96 h-64 relative bg-gray-300">
          <Image
            alt={data?.dish?.name}
            src={imageSrc}
            layout="fill"
            objectFit="cover"
          />
        </figure>
      )}
    </Layout>
  )
}

export default ProductDetailPage
