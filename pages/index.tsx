import { Layout } from 'components/Layout/Layout'
import Link from 'next/link'

export default function HomePage(): JSX.Element {
  return (
    <Layout className="text-center">
      <h1 className="text-5xl mt-4 mb-8">
        Welcome to our very exciting home page
      </h1>
      <Link href="/menu">
        <a className="underline">Check the menu</a>
      </Link>
    </Layout>
  )
}
