import type { AppProps } from 'next/app'
import { ApolloProvider } from '@apollo/client'
import apolloClient from 'utils/apollo-client'
import { CartContextProvider } from 'utils/CartContext/CartContext'
import 'styles/globals.css'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider client={apolloClient}>
      <CartContextProvider>
        <Component {...pageProps} />
      </CartContextProvider>
    </ApolloProvider>
  )
}

export default MyApp
