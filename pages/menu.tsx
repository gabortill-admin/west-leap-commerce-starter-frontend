import { Layout } from 'components/Layout/Layout'
import { MenuList } from 'components/MenuList/MenuList'

export default function MenuPage(): JSX.Element {
  return (
    <Layout>
      <MenuList />
    </Layout>
  )
}
