import { NextApiRequest, NextApiResponse } from 'next'
import { CartItemType } from 'utils/CartContext/CartContext'
import { validatePayload } from '../../utils/Payment/validate-payload'
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY)

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
): Promise<any> {
  if (req.method === 'POST') {
    const { payload } = req.body
    const calculateOrderAmount = (payload: CartItemType[]) => {
      const orderAmount = payload.reduce((accumulator, currentValue) => {
        return (
          accumulator +
          currentValue.unitPrice * 100 * currentValue.numberOfPieces
        )
      }, 0)

      return orderAmount
    }

    try {
      // validate the payload
      const isPayloadValid = await validatePayload({
        collection: 'dishes',
        payload,
      })

      if (!isPayloadValid) {
        // TODO
        // log it
        return res
          .status(500)
          .json({ success: false, statusCode: 500, message: '🤨' })
      }

      // Create a PaymentIntent with the order amount and currency
      const paymentIntent = await stripe.paymentIntents.create({
        amount: calculateOrderAmount(payload),
        currency: 'eur',
      })

      res.status(200).json({
        success: true,
        paymentIntent,
      })
    } catch (error) {
      res
        .status(500)
        .json({ success: false, statusCode: 500, message: error.message })
    }
  } else {
    res.setHeader('Allow', 'POST')
    res.status(405).end('Method Not Allowed')
  }
}
