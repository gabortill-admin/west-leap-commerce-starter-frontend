import { NextPage } from 'next'
import { useContext, useEffect, useState } from 'react'
import { CartContext } from 'utils/CartContext/CartContext'
import { useRouter } from 'next/router'
import { Loading } from 'components/Loading/Loading'
import { ErrorMessage } from 'components/ErrorMessage/ErrorMessage'
import { useOrderQuery } from 'pages/api/order.graphql'
import Link from 'next/link'
import { Layout } from 'components/Layout/Layout'

const OrderSuccessPage: NextPage = () => {
  const router = useRouter()
  const { emptyCart } = useContext(CartContext)
  const [variables, setVariables] = useState({ id: '' })
  const { data, loading, error } = useOrderQuery({ variables })

  // set query variables
  useEffect(() => {
    const queryId = router.query.id || ''
    const id = Array.isArray(queryId) ? queryId[0] : queryId

    if (router.query.id) {
      setVariables({ id })
    }
  }, [router])

  // empty the cart
  useEffect(() => {
    if (emptyCart) {
      emptyCart()
    }
  }, [emptyCart])

  if (loading) {
    return <Loading />
  }

  if (error) {
    return <ErrorMessage />
  }

  return (
    <Layout>
      <h1>Thanks for your purchase!</h1>
      <p>Your order number is {data?.order?.id}</p>
      <Link href="/">Go to home page</Link>
    </Layout>
  )
}

export default OrderSuccessPage
