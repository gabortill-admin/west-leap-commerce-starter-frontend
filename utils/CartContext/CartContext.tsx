import { createContext, useCallback, useEffect, useState } from 'react'
import Cookie from 'js-cookie'

export type CartItemType = {
  id: string
  name: string
  unitPrice: number
  numberOfPieces: number
}

export type CartContextType = {
  addItemsToCart: (item: CartItemType) => void
  cartItems: CartItemType[]
  totalNumberOfItemsInCart: number
  totalAmount: number
  emptyCart: () => void
  isCartEmpty: boolean
}

// define context with default values
export const CartContext = createContext<CartContextType>({
  addItemsToCart: () => {},
  cartItems: [],
  totalNumberOfItemsInCart: 0,
  emptyCart: () => {},
  isCartEmpty: true,
  totalAmount: 0,
})

// storing state && handling operations
export const CartContextProvider: React.FC = ({ children }) => {
  const cartInitialValues = JSON.parse(Cookie.get('cart') || '[]')
  const [cartItems, setCartItems] = useState<CartItemType[]>(cartInitialValues)
  const [totalNumberOfItemsInCart, setTotalNumberOfItemsInCart] = useState(0)
  const [totalAmount, setTotalAmount] = useState(0)
  const [isCartEmpty, setIsCartEmpty] = useState(true)

  // check wether the cart is empty or not
  useEffect(() => {
    setIsCartEmpty(cartItems.length <= 0)
  }, [cartItems])

  // calculate the total amount
  useEffect(() => {
    if (cartItems.length === 0) {
      setTotalAmount(0)
    } else {
      const total = cartItems.reduce(
        (accumulator: number, currentValue: CartItemType) => {
          return (
            accumulator + currentValue.unitPrice * currentValue.numberOfPieces
          )
        },
        0
      )

      setTotalAmount(total)
    }
  }, [cartItems])

  // calculate the total number of items in the cart
  useEffect(() => {
    if (cartItems.length === 0) {
      setTotalNumberOfItemsInCart(0)
    } else {
      const total = cartItems.reduce(
        (accumulator: number, currentValue: CartItemType) => {
          return accumulator + currentValue.numberOfPieces
        },
        0
      )

      setTotalNumberOfItemsInCart(total)
    }
  }, [cartItems])

  const addItemsToCart = useCallback((addedItem: CartItemType) => {
    setCartItems((cartItems) => {
      const copiedCartItemsWithoutReference = JSON.parse(
        JSON.stringify([...cartItems, addedItem])
      )
      const updatedCart = copiedCartItemsWithoutReference.reduce(
        (accumulator: CartItemType[], currentValue: CartItemType) => {
          const currentIndexInCart = accumulator.findIndex(
            (item) => item.id === currentValue.id
          )
          const isCurrentItemInCart = Boolean(currentIndexInCart !== -1)

          if (isCurrentItemInCart) {
            accumulator[currentIndexInCart].numberOfPieces +=
              currentValue.numberOfPieces
          } else {
            accumulator.push(currentValue)
          }

          return accumulator
        },
        []
      )

      Cookie.set('cart', updatedCart)

      return updatedCart
    })
  }, [])

  const emptyCart = useCallback(() => {
    setCartItems([])
  }, [])

  return (
    <CartContext.Provider
      value={{
        cartItems,
        addItemsToCart,
        totalNumberOfItemsInCart,
        totalAmount,
        emptyCart,
        isCartEmpty,
      }}
    >
      {children}
    </CartContext.Provider>
  )
}
