import { CartItemType } from 'utils/CartContext/CartContext'

type ValidatePayloadProps = {
  collection: string
  payload: CartItemType[]
}

type ItemFromStrapiProps = {
  id: string
}

export const validatePayload = async ({
  payload,
  collection,
}: ValidatePayloadProps) => {
  const queryParam = payload
    .map((cartItem) => `_where[id]=${cartItem.id}`)
    .join('&')
  const response = await fetch(
    `${process.env.NEXT_PUBLIC_IMG_ORIGIN}/${collection}?${queryParam}`
  )
  const itemsFromStrapi = await response.json()

  const areTheSameLength = payload.length === itemsFromStrapi.length

  const arePricesTheSame = payload.every((cartItem) => {
    const matchingStrapiItem = itemsFromStrapi.find(
      (itemFromStrapi: ItemFromStrapiProps) => itemFromStrapi.id === cartItem.id
    )

    if (!matchingStrapiItem) {
      return false
    }

    const strapiItemPrice = matchingStrapiItem.price || null

    return cartItem.unitPrice === strapiItemPrice
  })

  return areTheSameLength && arePricesTheSame
}
