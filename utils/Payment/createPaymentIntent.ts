import { CartItemType } from 'utils/CartContext/CartContext'

type CreatePaymentIntentProps = {
  payload: CartItemType[]
}

export const createPaymentIntent = async ({
  payload,
}: CreatePaymentIntentProps) => {
  const response = await fetch('/api/create-payment-intent', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ payload }),
  })
  const paymentIntent = await response.json()

  if (!paymentIntent.success) {
    return false
  }

  const { client_secret: clientSecret, id } = paymentIntent.paymentIntent

  return { clientSecret, id }
}
