const stripeErrors = [
  { code: 'card_declined', message: { en: 'The card was declined' } },
  { code: 'expired_card', message: { en: 'The card has expired' } },
  {
    code: 'incomplete_cvc',
    message: { en: 'The card’s security code is incomplete' },
  },
  {
    code: 'incomplete_expiry',
    message: { en: 'The card’s expiration date is incomplete' },
  },
  {
    code: 'incomplete_number',
    message: { en: 'The card number is incomplete' },
  },
  {
    code: 'incorrect_cvc',
    message: { en: 'The card’s security code is incorrect' },
  },
  {
    code: 'incorrect_number',
    message: { en: 'The card number is incorrect' },
  },
  {
    code: 'incorrect_zip',
    message: { en: 'The card’s zip code failed validation' },
  },
  {
    code: 'invalid_cvc',
    message: { en: 'The card’s security code is invalid' },
  },
  {
    code: 'invalid_expiry_month',
    message: { en: 'The card’s expiration month is invalid' },
  },
  {
    code: 'invalid_expiry_year',
    message: { en: 'The card’s expiration year is invalid' },
  },
  {
    code: 'invalid_expiry_year_past',
    message: { en: 'The card’s expiration year is in the past' },
  },
  {
    code: 'invalid_number',
    message: { en: 'The card number is not a valid credit card number' },
  },
  {
    code: 'missing',
    message: {
      en: 'There is no card on a customer that is being charged',
    },
  },
  {
    code: 'processing_error',
    message: { en: 'An error occurred while processing the card' },
  },
]

const unknownError = {
  message: { en: 'An error occured' },
}

type getPaymentErrorMessageProps = {
  code: string
}

export const getPaymentErrorMessage = ({
  code,
}: getPaymentErrorMessageProps) => {
  const error = stripeErrors.find((error) => error.code === code)
  const isErrorDefined = Boolean(error)

  return isErrorDefined ? error?.message.en : unknownError.message.en
}
