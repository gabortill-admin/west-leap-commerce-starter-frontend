export const extractId = (url: string): string | null => {
  const match = url.match(/-[a-z0-9]+$/)

  return match ? match[0].replace('-', '') : null
}
