const domains =
  process.env.NODE_ENV === 'development'
    ? ['localhost']
    : [process.env.NEXT_PUBLIC_API_URL]

module.exports = {
  images: {
    domains,
  },

  reactStrictMode: true,

  webpack: (config, options) => {
    config.module.rules.push({
      test: /\.graphql$/,
      exclude: /node_modules/,
      use: [options.defaultLoaders.babel, { loader: 'graphql-let/loader' }],
    })

    return config
  },
}
