# Commerce starter repo using NextJS, Typescript and GraphQL

## How to add Tailwind CSS support

https://tailwindcss.com/docs/guides/nextjs

### Setting up Tailwind CSS

```
npm install -D tailwindcss@latest postcss@latest autoprefixer@latest
```

### Create your configuration files

```
npx tailwindcss init -p
```

### Configure `tailwind.config.js` file to remove unused styles in production

```js
module.exports = {
   purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
```

### Create `styles/globals.css` file, then fill up with the following

```css
@tailwind base;
@tailwind components;
@tailwind utilities;
```

### Import global CSS to `pages/_app.tsx` file

```tsx
import 'styles/globals.css'
```

## How it's made

### Initialize the repo using NextJS starter with typescript support

```
npx create-next-app west-leap-commerce-starter-frontend --typescript
```

### Go into the newly created `west-leap-commerce-starter-frontend` folder

```
cd west-leap-commerce-starter-frontend
```

### Create master branch

```
git checkout -b master
```

### Delete the main branch

```
git branch -d main
```

### Delete the `pages/api` folder

```
rm -rf pages/api
```

### Delete the `styles` folder

```
rm -rf styles
```

### Delete the `public` folder

```
rm -rf public
```

### Create `.nvmrc` file containing the required NodeJS version

```
echo "14.17.1" > .nvmrc
```

### Type check

#### Extend `tsconfig.json` to specify the root folder as base directory to resolve non-relative module names

```ts
{
  "compilerOptions": {
    "baseUrl": ".",
```

#### Extend `./package.json` scripts

```json
{
  "scripts": {
    "type-check": "tsc --pretty --noEmit"
```

### Prettier

#### Install prettier

```
npm i -D prettier
```

#### Create `.prettierrc` file and fill with the following options

```json
{
  "semi": false,
  "singleQuote": true
}
```

#### Create a `.prettierignore` file and fill up with the following

```
node_modules/
.next/
package-lock.json
public/
```

#### Extend `package.json` scripts

```json
{
  "scripts": {
    "format": "prettier --write \"{,!(node_modules|.cache)/**/}*.{js,jsx,ts,tsx,md}\"",
```

### Husky

https://typicode.github.io/husky/#/?id=install

#### Install husky

```
npm i -D husky
```

#### Enable Git hooks

```
npx husky install
```

#### Extend `package.json` scripts

```json
{
  "scripts": {
    "prepare": "husky install"
```

#### Add commands to the pre-commit hook

```
npx husky add .husky/pre-commit "npm run type-check"
npx husky add .husky/pre-commit "npm run format"
npx husky add .husky/pre-commit "npm run lint"
```

### Apollo Client

https://www.apollographql.com/blog/apollo-client/next-js/next-js-getting-started/

#### Install apollo client

```
npm i @apollo/client graphql
```

#### Create `utils/apollo-client.ts` file then fill up with the following

```ts
import { ApolloClient, InMemoryCache } from '@apollo/client'

const apolloClient = new ApolloClient({
  uri: process.env.NEXT_PUBLIC_API_URL,
  cache: new InMemoryCache(),
})

export default apolloClient
```

#### Override `pages/_app.tsx` file with the following

```tsx
import type { AppProps } from 'next/app'
import { ApolloProvider } from '@apollo/client'
import apolloClient from 'utils/apollo-client'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider client={apolloClient}>
      <Component {...pageProps} />
    </ApolloProvider>
  )
}
export default MyApp
```

#### Override `pages/index.tsx` with the following

```tsx
export default function HomePage() {
  return <h1>Very exciting home page</h1>
}
```

### graphql-let

https://www.npmjs.com/package/graphql-let

#### Install graphql-let with its peer dependencies together with GraphQL code generator plugins

```
npm i -D graphql-let @graphql-codegen/cli @graphql-codegen/typescript @graphql-codegen/import-types-preset @graphql-codegen/typescript-operations @graphql-codegen/typescript-react-apollo
```

#### Create `.graphql-let.yml` file and fill up with the following

```
schema: 'http://localhost:1337/graphql'
documents: '**/*.graphql'
plugins:
  - typescript-operations
  - typescript-react-apollo
cacheDir: generated
```

#### Exclude cacheDir in `tsconfig.json`

```json
{
  "excludes": ["generated"]
}
```

#### Extend `.gitignore`

```
# graphql-let
*.graphql.d.ts
*.graphqls.d.ts
generated/
```

#### Extend `next.config.js`

```js
module.exports = {
  webpack: (config, options) => {
    config.module.rules.push({
      test: /\.graphql$/,
      exclude: /node_modules/,
      use: [options.defaultLoaders.babel, { loader: 'graphql-let/loader' }],
    })

    return config
  },
```
